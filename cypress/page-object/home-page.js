class HomePage{

  get getStartedButton() {
    return cy.get("#get-started").click();
  }

  get headingText() {
    return cy.get("h1.elementor-heading-title");
  }

  get primaryMenu() {
    return cy.get("#primary-menu");
  }

  get menuItemListsEl() {
    return cy.get('#primary-menu [id*="menu-item"]');
  }

  open() {
    cy.visit("https://practice.automationbro.com");
  }
}

export default new HomePage