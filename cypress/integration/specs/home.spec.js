import homePage from "../../page-object/home-page";

describe('Home Tests', () => {
  it('open the home page and verify the url and the title', () => {
    // open the Home Page
    homePage.open();

    // assert the url
    cy.url().should("include", "automationbro");

    // assert the title
    cy.title().should("eq", "Practice E-Commerce Site – Automation Bro");

  });

  it('clicks the Get Started button and asserts the url', () => {
    // click the button
    homePage.getStartedButton;
    
    // assert the url
    cy.url().should("include", "automationbro");
  });

  it('gets the text of the heading and assert the value', () => {
    // get the text
    // cy.get("h1.elementor-heading-title").should($heading => {
    //   // console.log($heading.text());
    //   expect($heading.text()).to.eq("Think different. Make different.");
    // })

    // assert the value
    homePage.headingText
      .should("have.text", "Think different. Make different.")
      .and("have.class", "elementor-size-default");
  });

  it('verifies the text of the first menu link item', () => {
    homePage.primaryMenu.find("li").first().should("have.text", "Home");
  });

  it('verifies the length and the text of all the menu link items', () => {
    const menuLinksText = [
      "Home",
      "About",
      "Shop",
      "Blog",
      "Contact",
      "My account"
    ];

    homePage.menuItemListsEl.should('have.length', 6)
    homePage.menuItemListsEl.each((item, index, list) => {
      // cy.wrap(item).should('contain.text', menuLinksText[index]);
      expect(Cypress.$(item).text()).to.eq(menuLinksText[index]);
    });
  });
});