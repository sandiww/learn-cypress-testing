describe('Upload Tests', () => {
  it('upload file and assert file name', () => {
    cy.visit('https://practice.automationbro.com/cart');

    // upload file to the input field
    cy.get('input[type="file"]')
      .invoke('removeClass', 'file_input_hidden')
      .attachFile('1652256745878.jpg');
    
    // click on the upload button
    cy.get('input[value="Upload File"]')
      .click();
    
    // assert the file name
    cy.get('#wfu_messageblock_header_1_label_1', { timeout: 10000 }).should('contain', 'uploaded successfully');
  });
});