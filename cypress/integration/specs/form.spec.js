describe('Support Form Tests', () => {
  it('opens the support form page and fills in the form', () => {
    cy.visit("https://practice.automationbro.com/support-form/");
    // text input
    cy.get('#evf-680-field_lVizlNhYus-1').type('Sandi Suryadi');
    cy.get('#evf-680-field_XYnMdkQDKM-3').type('sandirpl3e@gmail.com');
    cy.get('#evf-680-field_xJivsqAS2c-2').type('Unable to access the app');

    // dropdown
    cy.get('#evf-680-field_82kaAPhrnW-6')
      .select('Technical Team');

    // checkbox
    cy.get('#evf-680-field_sOAJfxP1Lf-7 input').check([
      "Integration Issue",
      "Software Issue"
    ]);

    // date picker
    cy.get('#evf-680-field_s1KysSbUW6-8').click();
    cy.get('.dayContainer span:nth-child(15)').click();

    // text area
    cy.get('#evf-680-field_YalaPcQ0DO-4').type('Your app keeps crashing everytime!')

    // button
    cy.get('#evf-submit-680').click();

    // verify success message
    cy.get('[role="alert"]').should(
      "contain.text",
      "Thanks for contacting us! We will be in touch with you shortly."
    );
  });
});